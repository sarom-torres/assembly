
;--------------------------------------------------------------------------------------

	DISPLAY EQU P0
;--------------------------------------------------------------------------------------

	ORG 0000h
	MOV P2, #00h

INICIO:
	;0
	MOV DISPLAY, #00000011b
	REPT 5
	CALL DELAY
	ENDM
	
	;1
	MOV DISPLAY, #10011111b
	REPT 5
	CALL DELAY
	ENDM

	;2
	MOV DISPLAY, #00100101b
	REPT 5
	CALL DELAY
	ENDM

	;3
	MOV DISPLAY, #00001101b
	REPT 5
	CALL DELAY
	ENDM
	
	;4
	MOV DISPLAY, #10011001b
	REPT 5
	CALL DELAY
	ENDM

	;5
	MOV DISPLAY, #01001001b
	REPT 5
	CALL DELAY
	ENDM

	;6
	MOV DISPLAY, #01000001b
	REPT 5
	CALL DELAY
	ENDM
	
	;7
	MOV DISPLAY, #00011111b
	REPT 5
	CALL DELAY
	ENDM

	;8
	MOV DISPLAY, #00000001b
	REPT 5
	CALL DELAY
	ENDM

	;9
	MOV DISPLAY, #00011001b
	REPT 5
	CALL DELAY
	ENDM

	INC P2

	JMP INICIO

	
;_______________SUBROTINAS_____________________________________________________________
DELAY:
	SETB TR2
	JNB TF2, $
	CLR TR2
	CLR EXF2
	CLR TF2
	RET

	END