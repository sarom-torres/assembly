# Assembly

Repositório utilizado para exemplos e exercícios na disciplina de **Microprocessadores** 2018-2 (MIC-29004) do curso de Engenharia de Telecomunicações.

## Conteúdo

- Ferramentas de programação assembly
- Linguagem assembly: tipos, formatos de instruções, modos de endereçamento, pilhas e subrotinas
- Sistema de Clock e Reset 
- Ciclos de execução
- Contadores e Temporizadores
- Sistema de Interrupção
- Interfaceamento com Periféricos: portas paralelas e seriais
