;------------------------| DEFINIÇAO DE HARDWARE |

	LED_VERMELHO EQU P2
	LED_VERDE EQU P0

;------------------------| PONTEIRO DE RESET |
	ORG 0000h
	JMP INICIO


	
;-------------------------| INICIO|

INICIO:

	MOV P2, #00h
	MOV P3, #00h
	
	MOV LED_VERDE, #0FFh
	REPT 5
	CALL DELAY
	ENDM

	MOV LED_VERMELHO, #00h
	REPT 5
	CALL DELAY
	ENDM
	
	MOV LED_VERDE, #00h
	REPT 5
	CALL DELAY
	ENDM

	MOV LED_VERMELHO, #0FFh
	REPT 5
	CALL DELAY
	ENDM
	

	
	
	
	JMP INICIO





;--------------------------|SUBROTINAS|

DELAY:
	SETB TR2
	JNB TF2, $
	CLR TR2
	CLR EXF2
	CLR TF2
	RET
	



;-------------------------------------\ FIM |
	END